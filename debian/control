Source: teem
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Dominique Belhachemi <domibel@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               cmake,
               zlib1g-dev,
               libpng-dev,
               libbz2-dev
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/science-team/teem
Vcs-Git: https://salsa.debian.org/science-team/teem.git
Homepage: http://teem.sourceforge.net/
Rules-Requires-Root: no

Package: libteem-dev
Architecture: any
Section: libdevel
Depends: libteem2 (= ${binary:Version}),
         ${misc:Depends}
Conflicts: libteem1-dev
Provides: libteem1-dev
Replaces: libteem1-dev
Description: Tools to process and visualize scientific data and images - development
 Teem is a coordinated group of libraries for representing, processing, and
 visualizing scientific raster data. Teem includes command-line tools that
 permit the library functions to be quickly applied to files and streams,
 without having to write any code. The most important and useful libraries in
 Teem are:
 .
  * Nrrd (and the unu command-line tool on top of it) supports a range of
    operations for transforming N-dimensional raster data (resample, crop,
    slice, project, histogram, etc.), as  well as the NRRD file format for
    storing arrays and their meta-information.
  * Gage: fast convolution-based measurements at arbitrary point locations in
    volume datasets (scalar, vector, tensor, etc.)
  * Mite: a multi-threaded ray-casting volume render with transfer functions
    based on any quantity Gage can measure
  * Ten: for estimating, processing, and visualizing diffusion tensor fields,
    including fiber tractography methods.
 .
 This package provides the Teem header files required to compile C++ programs
 that use Teem to do 3D visualisation.

Package: teem-apps
Architecture: any
Depends: libteem2 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: Tools to process and visualize scientific data and images - command line tools
 Teem is a coordinated group of libraries for representing, processing, and
 visualizing scientific raster data. Teem includes command-line tools that
 permit the library functions to be quickly applied to files and streams,
 without having to write any code. The most important and useful libraries in
 Teem are:
 .
  * Nrrd (and the unu command-line tool on top of it) supports a range of
    operations for transforming N-dimensional raster data (resample, crop,
    slice, project, histogram, etc.), as  well as the NRRD file format for
    storing arrays and their meta-information.
  * Gage: fast convolution-based measurements at arbitrary point locations in
    volume datasets (scalar, vector, tensor, etc.)
  * Mite: a multi-threaded ray-casting volume render with transfer functions
    based on any quantity Gage can measure
  * Ten: for estimating, processing, and visualizing diffusion tensor fields,
    including fiber tractography methods.
 .
 This package contains some simple command-line tools which provide fast and
 easy access to the functionality in the various libraries.

Package: libteem2
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Tools to process and visualize scientific data and images - runtime
 Teem is a coordinated group of libraries for representing, processing, and
 visualizing scientific raster data. Teem includes command-line tools that
 permit the library functions to be quickly applied to files and streams,
 without having to write any code. The most important and useful libraries in
 Teem are:
 .
  * Nrrd (and the unu command-line tool on top of it) supports a range of
    operations for  transforming N-dimensional raster data (resample, crop,
    slice, project, histogram, etc.), as  well as the NRRD file format for
    storing arrays and their meta-information.
  * Gage: fast convolution-based measurements at arbitrary point locations in
    volume datasets (scalar, vector, tensor, etc.)
  * Mite: a multi-threaded ray-casting volume render with transfer functions
    based on any quantity Gage can measure
  * Ten: for estimating, processing, and visualizing diffusion tensor fields,
    including fiber tractography methods.
 .
 This package provides the runtime files required to run programs
 that use Teem to do 3D visualisation.
